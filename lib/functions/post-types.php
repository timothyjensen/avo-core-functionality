<?php
/**
 * Post Types
 *
 * This file registers any custom post types
 *
 * @package      Core_Functionality
 * @since        1.0.0
 * @author       Tim Jensen <tim@timjensen.us>
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */


add_action( 'init', 'tj_register_testimonial_post_type' );
/**
 * Create Testimonial post type
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function tj_register_testimonial_post_type() {
	$labels = array(
		'name' => 'Testimonials',
		'singular_name' => 'Testimonial',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Testimonial',
		'edit_item' => 'Edit Testimonial',
		'new_item' => 'New Testimonial',
		'view_item' => 'View Testimonial',
		'search_items' => 'Search Testimonials',
		'not_found' =>  'No Testimonials found',
		'not_found_in_trash' => 'No Testimonials found in trash',
		'parent_item_colon' => '',
		'menu_name' => 'Testimonials'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'menu_icon'		=> 'dashicons-thumbs-up',
		'supports' => array('title','thumbnail','editor', 'page-attributes')
	);

	register_post_type( 'testimonials', $args );
}

add_action( 'init', 'tj_register_faq_post_type' );
/**
 * Create FAQ post type
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function tj_register_faq_post_type() {
	$labels = array(
		'name' => 'FAQs',
		'singular_name' => 'FAQ',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New FAQ',
		'edit_item' => 'Edit FAQ',
		'new_item' => 'New FAQ',
		'view_item' => 'View FAQ',
		'search_items' => 'Search FAQs',
		'not_found' =>  'No FAQs found',
		'not_found_in_trash' => 'No FAQs found in trash',
		'parent_item_colon' => '',
		'menu_name' => 'FAQs'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'menu_icon'		=> 'dashicons-welcome-write-blog',
		'taxonomies'    => array( 'questions' ),
		'supports' => array( 'title', 'page-attributes' )
	);

	register_post_type( 'faqs', $args );
}

add_action( 'init', 'tj_register_press_release_post_type' );
/**
 * Create Press Release post type
 *
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function tj_register_press_release_post_type() {
    $labels = array(
        'name' => 'Press Releases',
        'singular_name' => 'Press Release',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Press Release',
        'edit_item' => 'Edit Press Release',
        'new_item' => 'New Press Release',
        'view_item' => 'View Press Release',
        'search_items' => 'Search Press Releases',
        'not_found' =>  'No Press Releases found',
        'not_found_in_trash' => 'No Press Releases found in trash',
        'parent_item_colon' => '',
        'menu_name' => 'Press Releases'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon'		=> 'dashicons-megaphone',
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'page-attributes', 'genesis-seo', 'genesis-layouts'),
    );

    register_post_type( 'press-release', $args );
}
