<?php
/**
 * Shortcodes
 *
 * This file registers any custom post types
 *
 * @package      Core_Functionality
 * @since        1.0.0
 * @author       Tim Jensen <tim@timjensen.us>
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */


add_shortcode( 'avo-list-faqs', 'avo_list_faqs_shortcode' );
/**
 * Shortcode to render a list of FAQs
 *
 * @return string
 */
function avo_list_faqs_shortcode() {

	//* Get array of Post IDs that have a meta key/value for 'tj_headline_is_video'
	$args = array(
		'numberposts' 	=> 5,
		'order' 		=> 'DESC',
		'orderby' 		=> 'menu_order',
		'post_type' 	=> 'faqs',
		'post_status' 	=> 'publish',
	);

	$faqs = wp_get_recent_posts( $args );

	$html_string = '<div class="avocado-faqs">';

	foreach ( $faqs as $key => $faq ) {
		$count = $key + 1;
		$post_id = $faq['ID'];
		$question = apply_filters( 'the_content', get_post_meta( $post_id, 'avo_faq_question', true ) );
		$answer = apply_filters( 'the_content', get_post_meta( $post_id, 'avo_faq_answer', true ) );

		$html_string .= sprintf( '<div id="%s" class="%s"><button class="faq-toggle fa"></button><div class="faq-toggle-container"><div class="faq-question">%s</div><div class="faq-answer">%s</div></div></div>', "faq-{$count}", 'faq-wrap flex-container', $question, $answer );
	}

	$html_string .= '</div>';

	return $html_string;
}

add_shortcode( 'avo-list-testimonials', 'avo_do_testimonials_shortcode' );
/**
 * List the testimonials
 *
 * @return string   Testimonials
 */
function avo_do_testimonials_shortcode() {
    $args = array(
        'post_type'		=> 'testimonials',
        'post_status'	=> 'publish',
        'numberposts'   => 3,
        'order'			=> 'DESC',
        'orderby'		=> 'menu_order'
    );

    $testimonials = wp_get_recent_posts( $args );

    $testimonial_list = '<div id="avo-testimonials"><div class="wrap">';

    foreach ( $testimonials as $key => $testimonial ) {
        $reviewer_name = get_post_meta( $testimonial['ID'], "avo_reviewer_name", true );
        $reviewer_location = get_post_meta( $testimonial['ID'], "avo_reviewer_location", true );
        $testimonial_content = '&ldquo;' . $testimonial['post_content'] . '&rdquo;';
        $reviewer_rating = get_post_meta( $testimonial['ID'], "avo_reviewer_rating", true );
        $whole_stars = array( 1, 1, 2, 2, 3, 3, 4, 4, 5 );

        $star_rating = '';
        for ( $i = 0; $i < $whole_stars[$reviewer_rating]; $i++ ) {
            $star_rating .= '<span class="fa fa-star"></span>';
        }

        if ( ! 0 == $reviewer_rating % 2 ) {
            $star_rating .= '<span class="fa fa-star-half-o"></span>';
        }

        if ( 0 === $key ) {
            $testimonial_list .= sprintf( '<div class="avo-featured-testimonial"><h2 class="row-heading">%s</h2><p class="avo-testimonial-source"><span class="pipe-separator-after">%s</span>%s</p><div class="avo-star-rating">%s</div></div>', esc_html( $testimonial_content ), esc_html( $reviewer_name ), esc_html( $reviewer_location ), $star_rating );
        } else {
            $testimonial_list .= sprintf( '<div class="avo-basic-testimonial"><div class="avo-testimonial-content">%s</div><div class="avo-testimonial-source"><span class="pipe-separator-after">%s</span>%s</div><div class="avo-star-rating">%s</div></div>', wp_kses_post( apply_filters( 'the_content', $testimonial_content ) ), esc_html( $reviewer_name ), esc_html( $reviewer_location ), $star_rating );
        }
    }

    $testimonial_list .= '</div></div>';

    return $testimonial_list;
}
