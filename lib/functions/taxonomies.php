<?php
/**
 * Taxonomies
 *
 * This file registers any custom taxonomies
 *
 * @package      Core_Functionality
 * @since        1.0.0
 */

add_action( 'init', 'tj_register_press_release_category_taxonomy' );
/**
 * Create Press Release Category Taxonomy
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function tj_register_press_release_category_taxonomy() {
    $labels = array(
        'name' => 'Press Release Category',
        'singular_name' => 'Press Release Category',
        'search_items' =>  'Search Press Release Categories',
        'all_items' => 'All Press Release Categories',
        'parent_item' => 'Parent Press Release Category',
        'parent_item_colon' => 'Parent Press Release Category:',
        'edit_item' => 'Edit Press Release Category',
        'update_item' => 'Update Press Release Category',
        'add_new_item' => 'Add New Press Release Category',
        'new_item_name' => 'New Press Release Category Name',
        'menu_name' => 'Press Release Categories'
    );

    $args = array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
    );

    register_taxonomy( 'press-release-category', array( 'press-release' ), $args );
    register_taxonomy_for_object_type( 'press-release-category', 'press-release' );
}

//add_action( 'init', 'tj_register_questions_taxonomy' );
/**
 * Create Question Category Taxonomy
 * @since 1.0.0
 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function tj_register_questions_taxonomy() {
	$labels = array(
		'name' => 'Question Category',
		'singular_name' => 'Question Category',
		'search_items' =>  'Search Question Categories',
		'all_items' => 'All Question Categories',
		'parent_item' => 'Parent Question Category',
		'parent_item_colon' => 'Parent Question Category:',
		'edit_item' => 'Edit Question Category',
		'update_item' => 'Update Question Category',
		'add_new_item' => 'Add New Question Category',
		'new_item_name' => 'New Question Category Name',
		'menu_name' => 'Question Categories'
	);

    $args = array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
    );

	register_taxonomy( 'questions', array( 'faqs' ), $args );
    register_taxonomy_for_object_type( 'questions', 'faqs' );
}
