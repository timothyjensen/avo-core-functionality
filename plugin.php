<?php
/**
 * Plugin Name: Avocado Core Functionality
 * Plugin URI: https://github.com/timothyjensen/Core-Functionality
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 1.0.0
 * Author: Tim Jensen
 * Author URI: http://www.timjensen.us
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


if ( ! defined( 'WPINC' ) ) {
	die;
}

//* Plugin Directory
define( 'TJ_DIR', dirname( __FILE__ ) );

//* Post Types
include_once( TJ_DIR . '/lib/functions/post-types.php' );

//* Shortcodes
include_once( TJ_DIR . '/lib/functions/shortcodes.php' );

//* Taxonomies
include_once( TJ_DIR . '/lib/functions/taxonomies.php' );
